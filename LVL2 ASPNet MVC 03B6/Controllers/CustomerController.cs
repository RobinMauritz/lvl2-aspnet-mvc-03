﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASPNet_MVC_03B6.Models;

namespace LVL2_ASPNet_MVC_03B6.Controllers
{

    public class CustomerController : Controller
    {
        Db_CustomerEntities db = new Db_CustomerEntities();
        Db_ErrorLogEntities1 errDB = new Db_ErrorLogEntities1(); //Disini saya membuat 

        // GET: Customer
        public ActionResult Index()
        {
            return View(db.Tbl_Customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(db.Tbl_Customer.Where(b => b.Id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Tbl_Customer customer)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Tbl_Customer.Add(customer);
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception errMsg)
                {
                    var errorPosition = "Create Customer";
                    tbl_errorLog error = new tbl_errorLog
                    {
                        errMsg = errMsg.Message,
                        errPos = errorPosition,
                        errTime = DateTime.Now
                    };

                    errDB.tbl_errorLog.Add(error);
                    errDB.SaveChanges();

                    return View();
                }
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.Tbl_Customer.Where(b => b.Id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Tbl_Customer customer)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(customer).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception errMsg)
                {
                    var errorPosition = "Edit Customer";
                    tbl_errorLog error = new tbl_errorLog
                    {
                        errMsg = errMsg.Message,
                        errPos = errorPosition,
                        errTime = DateTime.Now
                    };

                    errDB.tbl_errorLog.Add(error);
                    errDB.SaveChanges();

                    return View();
                }
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(db.Tbl_Customer.Where(b => b.Id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Tbl_Customer customer)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    customer = db.Tbl_Customer.Where(b => b.Id == id).FirstOrDefault();
                    db.Tbl_Customer.Remove(customer);
                    db.SaveChanges();

                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception errMsg)
                {
                    var errorPosition = "Delete Customer";
                    tbl_errorLog error = new tbl_errorLog
                    {
                        errMsg = errMsg.Message,
                        errPos = errorPosition,
                        errTime = DateTime.Now
                    };

                    errDB.tbl_errorLog.Add(error);
                    errDB.SaveChanges();

                    return View();
                }
            }
        }
    }
}
